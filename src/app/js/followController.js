const changeFollow = (e) => {
	const iconFollowIndex = e.className.indexOf('icon_follow_follow');

	if (iconFollowIndex > 0) {
		e.classList.remove('icon_follow_follow');
		e.classList.add('icon_follow_unfollow');
	} else {
		e.classList.remove('icon_follow_unfollow');
		e.classList.add('icon_follow_follow');
	}
}